package lab;

public class Circle {
	int radius;
	Point center;
	
	public Circle(int x, Point O) {
		radius = x;
		center = O;
			}
		public double area() {
			return 3.14 * radius * radius;

		}
		public double perimeter() {
			return 2 * radius * 3.14 ;
		}
		public boolean intersect(Circle O) {
			double d = ((center.xCoord - O.center.xCoord) * (center.xCoord - O.center.xCoord) + (center.yCoord - O.center.yCoord) * ( center.yCoord))* (0.5 );
			int sum = radius + O.radius;
			
			if (d <= sum )
				return true;
			return false;
			
		}

}
