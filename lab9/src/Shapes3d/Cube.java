package Shapes3d;

import Shapes2d.Square;

public class Cube  extends Square {
	
	public Cube() {
		super(1.0);
	}
	public Cube (double side) {
		super(side);
	}
	public double getSide() {
		return super.getSide();
	}
	public void setSide(double side) {
		 super.setSide(side);
	}
	public double area() {
		return 6 * super.area();
	}
	public double volume() {
		return super.area() * getSide();
	}
	public String toString() {
		return "Cube of [side = " + this.getSide() + " ] ";
	}

}
