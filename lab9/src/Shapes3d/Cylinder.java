package Shapes3d;

import Shapes2d.Circle;

public class Cylinder extends Circle{
	private double height;
	
	public Cylinder() {
		super();
		height = 1.0;
	}
	public Cylinder (double radius , double height) {
		super(radius);
		this.height = height;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double area() {
		return (2 * super.area()) + (height * 2 * Math.PI * getRadius());
	}
	public double volume() {
		return 2 * super.area() * height;
	}
	public String toString() {
		return "Cylinder of [ radius = " + getRadius() + " and height = " + height + " ] ";
	}

}
