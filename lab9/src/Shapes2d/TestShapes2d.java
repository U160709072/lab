package Shapes2d;

public class TestShapes2d {

	public static void main(String[] args) {
		Square square = new Square(5.0);
		Circle circle = new Circle (3.6);
		System.out.println("The area of " + square.toString() + " is: " + square.area());
		System.out.println("The area of " + circle.toString() + " is: " + circle.area());
		

	}

}
