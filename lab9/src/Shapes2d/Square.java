package Shapes2d;

public class Square {
	private double side;
    
	public Square() {
		side = 1.0;
	}
	public Square (double side) {
		this.side = side;
	}


	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}
	public double area() {
		return side * side;
		
	}
	public String toString() {
		return "Squade  [ side = " + this.side + "]";
	}
	

}
