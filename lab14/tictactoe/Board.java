package tictactoe;

public class Board {

	char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
	int currentPlayer = 1;

	int moveCount;

	public boolean isEnded() {
		return moveCount == 9;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void move(int row, int col) throws InvalidMoveException {
		try {
			if (board[row - 1][col - 1] != ' ')
				throw new InvalidMoveException("The location entered is already occupied, please try again.");

			board[row - 1][col - 1] = currentPlayer == 1 ? 'X' : 'O';
		} catch (IndexOutOfBoundsException e) {
			throw new InvalidMoveException(
					"The row and column pair that have been entered is out of the range of the board.");
		}
		currentPlayer = 3 - currentPlayer;
		moveCount++;
	}

	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("    1   2   3\n").append("   -----------\n");
		for (int row = 0; row < 3; ++row) {
			buf.append(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				buf.append("|");
				buf.append(" " + board[row][col] + " ");
				if (col == 2)
					buf.append("|");
			}

			buf.append("\n");
			buf.append("   -----------\n");
		}
		return buf.toString();
	}
}

class InvalidMoveException extends Exception {
	InvalidMoveException(String s) {
		super(s);
	}
}
