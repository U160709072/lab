package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);

		Board board = new Board();
		System.out.println(board);
		while (!board.isEnded()) {
			try {
				int player = board.getCurrentPlayer();
				int row;
				while (true) {
					try {
						System.out.print("Player " + player + " enter row number:");
						row = reader.nextInt();
						break;
					} catch (InputMismatchException e) {
						reader = new Scanner(System.in);
						System.out.println("A non-numerical value has been entered, please try again.");
					}
				}
				int col;
				while (true) {
					try {
						System.out.print("Player " + player + " enter column number:");
						col = reader.nextInt();
						break;
					} catch (InputMismatchException e) {
						reader = new Scanner(System.in);
						System.out.println("A non-numerical value has been entered, please try again.");
					}
				}
				board.move(row, col);
				System.out.println(board);
			} catch (InvalidMoveException x) {
				System.out.println(x.getMessage());
			}
		}

		reader.close();
	}

}

