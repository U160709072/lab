import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);

        int row,col;
        do{

		    System.out.print("Player 1 enter row number:");
		    row = reader.nextInt();
		    System.out.print("Player 1 enter column number:");
		    col = reader.nextInt();
		    board[row - 1][col - 1] = 'X';
		    printBoard(board);
        }while(!(row > 0 && row < 4 && col > 0 && col < 4 && board[row-1][col-1] == ' '));
        
        do{


		    System.out.print("Player 2 enter row number:");
		    row = reader.nextInt();
		    System.out.print("Player 2 enter column number:");
		    col = reader.nextInt();
		    board[row - 1][col - 1] = 'O';
		    printBoard(board);
        }while(!(row > 0 && row < 4 && col > 0 && col < 4 && board[row-1][col-1] == ' '));
    
		//reader.close();
	}

    public static boolean checkBoard(char[][] board, int rowLast, int colLast){
        char s = board[rowLast][colLast];
        boolean winFlag = true;
        for (int col = 0; col < 3; col++) {
            if (board[rowLast][col] != 0) {
                winFlag = false;
                break;
            }
        }
        if(winFlag)
            return true;

        winFlag = true;
        for (int row = 0;row < 3; row++) {
            if (board[row][colLast] != 0){
                winFlag = false;
                break;
            }
        }
        if(winFlag)
            return true;
        

 
    }
    public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
