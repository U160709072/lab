public class MyDateTime {
    MyDate myDate;
    MyTime myTime;

    MyDateTime(MyDate date, MyTime time) {
        myDate = date;
        myTime = time;
    }

    public void incrementDay() {
        myDate.incrementDay();
    }

    public void incrementHour(int i) {
        incrementMinute(i * 60);
    }

    public void decrementHour(int i) {
        decrementMinute(i * 60);
    }

    public void incrementMinute(int i) {
        int days = (myTime.mins + i) / 1440;
        if (days > 0)
            myDate.incrementDay(days);
        myTime.incrementMinute(i);
    }

    public void decrementMinute(int i) {
        int days = (myTime.mins - i) / 1440;
        int extra = (myTime.mins - i) % 1440;
        if (days < 0)
            days = -days;
        if (extra < 0)
            days++;
        if (days > 0)
            myDate.decrementDay(days);
        myTime.decrementMinute(i);
    }

    public void incrementYear(int i) {
        myDate.incrementYear(i);
    }

    public void decrementDay() {
        myDate.decrementDay();
    }

    public void decrementYear() {
        myDate.decrementYear();
    }

    public void decrementMonth() {
        myDate.decrementMonth();
    }

    public void incrementDay(int i) {
        myDate.incrementDay(i);
    }

    public void decrementMonth(int i) {
        myDate.decrementMonth(i);
    }

    public void decrementDay(int i) {
        myDate.decrementDay(i);
    }

    public void incrementMonth(int i) {
        myDate.incrementMonth(i);
    }

    public void decrementYear(int i) {
        myDate.decrementYear(i);
    }

    public void incrementMonth() {
        myDate.incrementMonth();
    }

    public void incrementYear() {
        myDate.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        boolean isBefore= myDate.isBefore(anotherDateTime.myDate);
        return isBefore || (myTime.mins < anotherDateTime.myTime.mins);
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        boolean isAfter= myDate.isAfter(anotherDateTime.myDate);
        return isAfter || (myTime.mins > anotherDateTime.myTime.mins);
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
       
        int days=myDate.dayDifference(anotherDateTime.myDate);

        int mins=myTime.minsDifference(anotherDateTime.myTime);
        if(anotherDateTime.myTime.mins < myTime.mins){
            days--;
            mins=1440-mins;
        }

        int hours=mins/60;
        int realMins=mins % 60;
        return (days>0? days+" day(s) ": "")+ (hours>0 ? hours+" hour(s) ":"")+ (realMins>0? realMins+" minute(s)":"");
    }

    public void incrementHour() {
        myTime.incrementHour();
    }

    public String toString() {
        return myDate.toString() + " " + myTime.toString();
    }
}
